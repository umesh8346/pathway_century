package centurybankapp.com.comcenturybankapp.Login.FAQ;


import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;



import java.util.HashMap;
import java.util.List;

import centurybankapp.com.comcenturybankapp.Login.Adapter.Expandablelistadapter;
import centurybankapp.com.comcenturybankapp.Login.Base;
import centurybankapp.com.comcenturybankapp.Login.FAQ.mvp.FAQPresenter;
import centurybankapp.com.comcenturybankapp.Login.FAQ.mvp.FAQView;

public class FAQActivity  extends Base implements FAQView{
    FAQPresenter faqPresenter;
    Expandablelistadapter expandableListAdapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDeps().inject(this);
        faqPresenter = new FAQPresenter(this,this);
        setContentView(faqPresenter);
    }

    @Override
    public void expandlistview(ExpandableListView exlist, List<String> parent, HashMap<String, List<String>> child) {
        expandableListAdapter = new Expandablelistadapter(this,parent, child);
        exlist.setAdapter(expandableListAdapter);
    }

    @Override
    public void settoolbar(Toolbar toolbar, String title) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }
}
