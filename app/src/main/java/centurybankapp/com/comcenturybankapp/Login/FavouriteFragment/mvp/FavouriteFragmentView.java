package centurybankapp.com.comcenturybankapp.Login.FavouriteFragment.mvp;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

public interface FavouriteFragmentView {
    void paybutton(View v,int position);
    void progressbar(ProgressBar progressbar, int visible);
    void  userfavorite(FavouriteResponse response, RecyclerView recyclerView);
}
