package centurybankapp.com.comcenturybankapp.Login.DashboardFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.io.File;

import javax.inject.Inject;

import centurybankapp.com.comcenturybankapp.Login.Adapter.DashboardRecycleAdapter;
import centurybankapp.com.comcenturybankapp.Login.DashboardFragment.mvp.AccountResponse;
import centurybankapp.com.comcenturybankapp.Login.DashboardFragment.mvp.DashboardFragmentPresenter;
import centurybankapp.com.comcenturybankapp.Login.DashboardFragment.mvp.DashboardFragmentView;
import centurybankapp.com.comcenturybankapp.Login.deps.DaggerDeps;
import centurybankapp.com.comcenturybankapp.Login.deps.Deps;
import centurybankapp.com.comcenturybankapp.Login.networking.NetworkModule;
import centurybankapp.com.comcenturybankapp.Login.networking.Service;


public class DashboardFargment extends Fragment implements DashboardFragmentView{
    DashboardFragmentView dashboardFragmentView;
    DashboardFragmentPresenter dashboardPresenter;
    Deps deps;
    @Inject
    Service service;
    DashboardRecycleAdapter dashboardRecycleAdapter;
    public static DashboardFargment newInstance() {
        DashboardFargment fragment = new DashboardFargment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        dashboardFragmentView = this;
        File cacheFile = new File(getActivity().getCacheDir(), "responses");
        deps = DaggerDeps.builder().networkModule(new NetworkModule(cacheFile)).build();
        deps.inject(DashboardFargment.this);
        dashboardPresenter=new DashboardFragmentPresenter(getActivity(),this,service);
        return dashboardPresenter;
    }

    @Override
    public void onSetProgressBarVisibility(ProgressBar progressBar, int visibility) {
        progressBar.setVisibility(visibility);
    }

    @Override
    public void getmyaccount(AccountResponse myaccountresponse, RecyclerView recyclerView) {
        dashboardRecycleAdapter=new DashboardRecycleAdapter(getActivity(),this);
        recyclerView.setAdapter(dashboardRecycleAdapter);
    }



    @Override
    public void onFailure(String appErrorMessage) {

    }

    @Override
    public void onchecknetwork(Boolean b) {

    }

    @Override
    public void paybbtn(View v, int position) {

    }

    @Override
    public void detailbtn(View v, int position) {

    }

    @Override
    public void statementbtn(View v, int position) {

    }

    @Override
    public void fundtranserbtn(View v, int position) {

    }
}
