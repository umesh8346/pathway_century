package centurybankapp.com.comcenturybankapp.Login;

import android.os.Bundle;

import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.comcenturybankapp.R;

import javax.inject.Inject;

import centurybankapp.com.comcenturybankapp.Login.networking.NetworkService;
import centurybankapp.com.comcenturybankapp.Login.networking.Response;
import centurybankapp.com.comcenturybankapp.Login.networking.Service;
import centurybankapp.com.comcenturybankapp.Login.presenter.LoginPresenterComp;
import centurybankapp.com.comcenturybankapp.Login.view.LoginView;


public class LoginActivity extends Base implements LoginView{

    LoginPresenterComp loginPresenter;

    @Inject
    public  Service service;

    @Inject
     public NetworkService networkService;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDeps().inject(this);
        loginPresenter = new LoginPresenterComp(this,this,service,networkService);
        setContentView(loginPresenter);
    }


    @Override
    public void onLoginResult(Boolean result, int code,String username,String passwrod) {
        if(result){

            loginPresenter.getuserlogin(username, passwrod);
        }
        else{
        }

    }

    @Override
    public void onSetProgressBarVisibility(int visibility,ProgressBar progressBar) {
        progressBar.setVisibility(visibility);
    }

    @Override
    public void isnotvalid(EditText ed,String error) {
        ed.setError(error);
    }



    @Override
    public void getusersuccess(Boolean b) {
if(b){
    //call new activity
}
    }



    @Override
    public void onFailure(String appErrorMessage) {
System.out.println("error=="+appErrorMessage);
    }

}
