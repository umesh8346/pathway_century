package centurybankapp.com.comcenturybankapp.Login.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.comcenturybankapp.R;

import centurybankapp.com.comcenturybankapp.Login.DashboardFragment.mvp.DashboardFragmentView;


public class DashboardRecycleAdapter extends RecyclerView.Adapter<DashboardRecycleAdapter.ViewHolder> {
Context mContext;
    DashboardFragmentView dasboardfargmentview;
    public DashboardRecycleAdapter(Context c,DashboardFragmentView dsahboarFragmentview) {
        this.mContext = c;
        this.dasboardfargmentview=dsahboarFragmentview;
    }

    @Override
    public DashboardRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        View vi = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.account_item_layout, parent, false);
        ViewHolder vh = new ViewHolder(vi);

        vh.btnpay= (Button)vi.findViewById(R.id.btnpay);
        vh.btndetail= (Button)vi.findViewById(R.id.btndetail);
        vh.btnstatement= (Button)vi.findViewById(R.id.btnstatement);
        vh.btnfundtransfer= (Button)vi.findViewById(R.id.btnfundtransfer);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.btnpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dasboardfargmentview.paybbtn(v,position);
            }
        });
        holder.btndetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dasboardfargmentview.detailbtn(v,position);
            }
        });
        holder.btnstatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dasboardfargmentview.statementbtn(v,position);
            }
        });
        holder.btnfundtransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dasboardfargmentview.fundtranserbtn(v,position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

Button btnpay,btndetail,btnstatement,btnfundtransfer;

        public ViewHolder(View v) {
            super(v);


        }

    }
}
