package centurybankapp.com.comcenturybankapp.Login.BranchAtm;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.comcenturybankapp.R;

import javax.inject.Inject;

import centurybankapp.com.comcenturybankapp.Login.Base;
import centurybankapp.com.comcenturybankapp.Login.BranchAtm.mvp.BranchAtmPresenter;
import centurybankapp.com.comcenturybankapp.Login.BranchAtm.mvp.BranchAtmView;
import centurybankapp.com.comcenturybankapp.Login.networking.Service;
public class BranchAtmActivity extends Base implements BranchAtmView {

    public BranchAtmPresenter branchAtmPresenter;
    @Inject
    Service service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDeps().inject(this);
        branchAtmPresenter = new BranchAtmPresenter(this,this,service);
        setContentView(branchAtmPresenter);

    }


    @Override
    public void settoolbar(Toolbar toolbar, String title) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void branchListview() {

    }
}
