package centurybankapp.com.comcenturybankapp.Login.BranchAtmListView.mvp;


import android.app.Activity;

import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.comcenturybankapp.R;

import centurybankapp.com.comcenturybankapp.Login.Adapter.BranchAtmAdapter;

import centurybankapp.com.comcenturybankapp.Login.networking.Service;

public class BranchAtmListPresenter extends FrameLayout implements View.OnClickListener {
    BranchAtmListView branchAtmListView;
    Toolbar toolbar;
    private FragmentActivity myContext;
    TabLayout viewPagerTab;
    ViewPager  viewPager;
    FragmentManager fragManager;
    BranchAtmAdapter branchAtmAdapter;
    Service service;
    ImageButton imagebuttonLocation;
    public BranchAtmListPresenter(@NonNull Activity activity, BranchAtmListView branchAtmListView, Service service) {
        super(activity);

        this.branchAtmListView=branchAtmListView;
        this.service=service;

        inflate(getContext(), R.layout.branch_atm_list_layout, this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        branchAtmListView.settoolbar(toolbar,"Branch and Atm Location");
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPagerTab = (TabLayout) findViewById(R.id.viewpagertab);
        myContext=(FragmentActivity) activity;
        fragManager = myContext.getSupportFragmentManager();
        viewPagerTab.addTab(viewPagerTab.newTab().setText("Branch"));
        viewPagerTab.addTab(viewPagerTab.newTab().setText("ATM"));
        imagebuttonLocation= (ImageButton) findViewById(R.id.imagebuttonLocation);
        imagebuttonLocation.setOnClickListener(this);
        branchAtmAdapter = new BranchAtmAdapter(fragManager,viewPagerTab.getTabCount());
        viewPager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(viewPagerTab));
        viewPager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(viewPagerTab));
        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(branchAtmAdapter);
        viewPagerTab.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

        });
    }


    @Override
    public void onClick(View v) {
        if(v==imagebuttonLocation){
branchAtmListView.getbranchatmlocation();
        }

    }
}
