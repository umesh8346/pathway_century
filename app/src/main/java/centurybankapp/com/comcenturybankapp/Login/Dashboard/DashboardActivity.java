package centurybankapp.com.comcenturybankapp.Login.Dashboard;


import android.os.Bundle;
import centurybankapp.com.comcenturybankapp.Login.Base;
import centurybankapp.com.comcenturybankapp.Login.Dashboard.mvp.DashboardPresenter;
import centurybankapp.com.comcenturybankapp.Login.Dashboard.mvp.DashboardView;

public class DashboardActivity extends Base  implements DashboardView{
   public  DashboardPresenter dashboardPresenter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDeps().inject(this);
        dashboardPresenter = new DashboardPresenter(this,this);
        setContentView(dashboardPresenter);
    }

    @Override
    public void settoolbar(android.support.v7.widget.Toolbar toolbar, String title) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }
}
