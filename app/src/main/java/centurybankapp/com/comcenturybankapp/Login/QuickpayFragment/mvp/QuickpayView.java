package centurybankapp.com.comcenturybankapp.Login.QuickpayFragment.mvp;

import android.view.View;

public interface QuickpayView {
    void ntcprepaidtopup();
    void ntcpostpaid();
    void ntcadsltopup();
}
