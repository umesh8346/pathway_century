package centurybankapp.com.comcenturybankapp.Login.Dashboard.mvp;


import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import android.widget.ProgressBar;


import com.comcenturybankapp.R;

import centurybankapp.com.comcenturybankapp.Login.Adapter.DashboardAdapter;
import centurybankapp.com.comcenturybankapp.Login.DashboardFragment.DashboardFargment;
import centurybankapp.com.comcenturybankapp.Login.QuickpayFragment.QuickPayFragment;


public class DashboardPresenter extends FrameLayout {
    Toolbar toolbar;
    ViewPager viewPager;
    TabLayout viewPagerTab;
    FragmentManager fragManager;
    ProgressBar progressBar;
    DashboardAdapter dashadpter;
    DashboardView dashboardView;
    private FragmentActivity myContext;
    public DashboardPresenter(@NonNull Activity activity,DashboardView dashboardView) {
        super(activity);
        this.dashboardView=dashboardView;
        inflate(getContext(), R.layout.dashboard_layout, this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPagerTab = (TabLayout) findViewById(R.id.viewpagertab);
        dashboardView.settoolbar(toolbar,"Dashboard");
        myContext=(FragmentActivity) activity;
        fragManager = myContext.getSupportFragmentManager();
        viewPagerTab.addTab(viewPagerTab.newTab().setText("Dashboard").setIcon(R.drawable.branches_atm));
        viewPagerTab.addTab(viewPagerTab.newTab().setText("Quickpay").setIcon(R.drawable.branches_atm));
        viewPagerTab.addTab(viewPagerTab.newTab().setText("favourites").setIcon(R.drawable.branches_atm));
        dashadpter = new DashboardAdapter(fragManager,viewPagerTab.getTabCount());
        viewPager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(viewPagerTab));
        viewPager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(viewPagerTab));
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(dashadpter);
        viewPagerTab.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

        });
    }


}

