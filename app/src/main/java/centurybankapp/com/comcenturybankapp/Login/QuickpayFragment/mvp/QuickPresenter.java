package centurybankapp.com.comcenturybankapp.Login.QuickpayFragment.mvp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.comcenturybankapp.R;


public class QuickPresenter extends FrameLayout implements View.OnClickListener {

    QuickpayView quickpayView;

    RelativeLayout relativelayoutntctopup;

    public QuickPresenter(@NonNull Context context,QuickpayView quickpayView) {
        super(context);
        this.quickpayView=quickpayView;
        inflate(getContext(), R.layout.quickpay_layout, this);

        relativelayoutntctopup=(RelativeLayout)findViewById(R.id.relativelayoutntctopup);
        relativelayoutntctopup.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

          if(v==relativelayoutntctopup){
              quickpayView.ntcprepaidtopup();
        }

    }
}
