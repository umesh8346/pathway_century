package centurybankapp.com.comcenturybankapp.Login.networking;

import java.io.IOException;
import java.net.HttpURLConnection;

import okhttp3.*;
import okhttp3.Response;



public class AccessTokenInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();
        Response response = chain.proceed(request);

        if (response.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {

        }
        return null;
    }
}
