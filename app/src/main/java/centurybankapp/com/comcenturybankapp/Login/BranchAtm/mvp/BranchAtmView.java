package centurybankapp.com.comcenturybankapp.Login.BranchAtm.mvp;

import android.support.v7.widget.Toolbar;

public interface BranchAtmView {
    void settoolbar(Toolbar toolbar, String title);
    void branchListview();
}
