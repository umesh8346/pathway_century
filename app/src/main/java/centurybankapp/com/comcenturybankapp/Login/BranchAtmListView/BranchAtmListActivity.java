package centurybankapp.com.comcenturybankapp.Login.BranchAtmListView;


import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import javax.inject.Inject;

import centurybankapp.com.comcenturybankapp.Login.Base;

import centurybankapp.com.comcenturybankapp.Login.BranchAtmListView.mvp.BranchAtmListPresenter;
import centurybankapp.com.comcenturybankapp.Login.BranchAtmListView.mvp.BranchAtmListView;
import centurybankapp.com.comcenturybankapp.Login.networking.Service;

public class BranchAtmListActivity extends Base  implements BranchAtmListView{
    public BranchAtmListPresenter branchAtmListPresenter;
    @Inject
    Service service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDeps().inject(this);
        branchAtmListPresenter = new BranchAtmListPresenter(this,this,service);
        setContentView(branchAtmListPresenter);

    }


    @Override
    public void getbranchatmlocation() {
        //startactivity

    }

    @Override
    public void settoolbar(Toolbar toolbar, String Title) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Title);
    }
}
