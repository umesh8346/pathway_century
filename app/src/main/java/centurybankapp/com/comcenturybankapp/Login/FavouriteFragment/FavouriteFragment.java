package centurybankapp.com.comcenturybankapp.Login.FavouriteFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.io.File;

import javax.inject.Inject;

import centurybankapp.com.comcenturybankapp.Login.Adapter.FavouriteRecycleAdapter;
import centurybankapp.com.comcenturybankapp.Login.FavouriteFragment.mvp.FavouriteFragmentPresenter;
import centurybankapp.com.comcenturybankapp.Login.FavouriteFragment.mvp.FavouriteFragmentView;
import centurybankapp.com.comcenturybankapp.Login.FavouriteFragment.mvp.FavouriteResponse;
import centurybankapp.com.comcenturybankapp.Login.deps.DaggerDeps;
import centurybankapp.com.comcenturybankapp.Login.deps.Deps;
import centurybankapp.com.comcenturybankapp.Login.networking.NetworkModule;
import centurybankapp.com.comcenturybankapp.Login.networking.Service;

public class FavouriteFragment extends Fragment implements FavouriteFragmentView {

    FavouriteFragmentView favouriteFragmentView;
    FavouriteFragmentPresenter  favouriteFragmentPresenter;
    Deps deps;
    @Inject
    Service service;
    FavouriteRecycleAdapter favouriteRecycleAdapter;
    FavouriteFragmentPresenter fac;    public static FavouriteFragment newInstance() {
        FavouriteFragment fragment = new FavouriteFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        favouriteFragmentView=this;
        File cacheFile = new File(getActivity().getCacheDir(), "responses");
        deps = DaggerDeps.builder().networkModule(new NetworkModule(cacheFile)).build();
        deps.inject(FavouriteFragment.this);
        favouriteFragmentPresenter=new FavouriteFragmentPresenter(getActivity(),this,service);
        return favouriteFragmentPresenter;
    }


    @Override
    public void paybutton(View v, int position) {

    }

    @Override
    public void progressbar(ProgressBar progressbar, int visible) {
        progressbar.setVisibility(visible);
    }

    @Override
    public void userfavorite(FavouriteResponse response, RecyclerView recyclerView) {
        favouriteRecycleAdapter=new FavouriteRecycleAdapter(getActivity(),this);
        recyclerView.setAdapter(favouriteRecycleAdapter);
    }
}
