package centurybankapp.com.comcenturybankapp.Login.FAQ.mvp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;


import com.comcenturybankapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FAQPresenter extends FrameLayout {
    FAQView faqView;
    ExpandableListView expandableListView;
    List<String> listDataHeader;

    HashMap<String, List<String>> listDataChild;
    Context context;
    Toolbar toolbar;
    public FAQPresenter(@NonNull Context context,FAQView faqView) {
        super(context);
        this.faqView=faqView;

        inflate(getContext(), R.layout.faq_layout, this);
        expandableListView=(ExpandableListView)findViewById(R.id.lvExp);
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        faqView.settoolbar(toolbar,"FAQ");
        prepareListData();
        faqView.expandlistview(expandableListView,listDataHeader,listDataChild);
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {

                return false;
            }
        });
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {

            }
        });


    }



    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();
        // Adding child data
        listDataHeader.add("How to reset my password?");
        listDataHeader.add("How Can i register to CCBL Mobile Banking");
        listDataHeader.add("Help Me on registrating new device");
        // Adding child data
        List<String> resetpasswrod = new ArrayList<>();
        resetpasswrod.add("-first please login into app");
        resetpasswrod.add("Go to setting icon");
        resetpasswrod.add("here yo reset password");
        List<String> registerCCBL = new ArrayList<>();
        registerCCBL.add("click on register");
        registerCCBL.add("fill the form");
        registerCCBL.add("subbmite");
        List<String> newdeviice = new ArrayList<>();
        newdeviice.add("Android ");
        newdeviice.add("Iphone");
        newdeviice.add("Nokia");
        listDataChild.put(listDataHeader.get(0), resetpasswrod);
        listDataChild.put(listDataHeader.get(1), registerCCBL);
        listDataChild.put(listDataHeader.get(2), newdeviice);
    }

}
