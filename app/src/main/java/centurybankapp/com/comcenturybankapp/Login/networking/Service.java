package centurybankapp.com.comcenturybankapp.Login.networking;


import centurybankapp.com.comcenturybankapp.Login.DashboardFragment.mvp.AccountResponse;
import centurybankapp.com.comcenturybankapp.Login.FavouriteFragment.mvp.FavouriteResponse;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class Service {
    private final NetworkService networkService;

    public Service(NetworkService networkService) {
        this.networkService = networkService;
    }

    public Subscription getuserlogin(final GetloginCallback callback ,String username, String password) {
        return networkService.getuserlogin(username,password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends centurybankapp.com.comcenturybankapp.Login.networking.Response>>() {
                    @Override
                    public Observable<? extends centurybankapp.com.comcenturybankapp.Login.networking.Response> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<centurybankapp.com.comcenturybankapp.Login.networking.Response>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(centurybankapp.com.comcenturybankapp.Login.networking.Response userloginresponse) {
                        callback.onSuccess(userloginresponse);

                    }
                });
    }
//user account favourite
    public Subscription getuseraccount(final GetAccountCallback callback,String UserID) {
        return networkService.getuseraccount(UserID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends AccountResponse>>() {
                    @Override
                    public Observable<? extends AccountResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<AccountResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(AccountResponse myaccountresponse) {
                        callback.onSuccess(myaccountresponse);

                    }
                });
    }



//user favoite

    public Subscription getuserfavourite(final GetFavourite callback,String UserID) {
        return networkService.getuserFavourite(UserID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends FavouriteResponse>>() {
                    @Override
                    public Observable<? extends FavouriteResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<FavouriteResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(FavouriteResponse userfavouritesesponse) {
                        callback.onSuccess(userfavouritesesponse);

                    }
                });
    }


    public interface GetloginCallback{
        void onSuccess(Response loginresponse);

        void onError(NetworkError networkError);
    }

    public interface GetAccountCallback{
        void onSuccess(AccountResponse loginresponse);

        void onError(NetworkError networkError);
    }

    public interface GetFavourite{
        void onSuccess(FavouriteResponse loginresponse);

        void onError(NetworkError networkError);
    }

}
