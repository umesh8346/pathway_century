package centurybankapp.com.comcenturybankapp.Login.DashboardFragment.mvp;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

public interface DashboardFragmentView {

    void onSetProgressBarVisibility(ProgressBar progressBar,int visibility);
    void getmyaccount(AccountResponse myaccountresponse, RecyclerView recyclerView);
    void onFailure(String appErrorMessage);
    void onchecknetwork(Boolean b);
    void paybbtn(View v,int position);
    void detailbtn(View v, int position);
    void statementbtn(View v, int position);
    void fundtranserbtn(View v, int position);
}
