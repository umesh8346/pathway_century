package centurybankapp.com.comcenturybankapp.Login.Adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import centurybankapp.com.comcenturybankapp.Login.BranchFragment.BranchFragment;
import centurybankapp.com.comcenturybankapp.Login.DashboardFragment.DashboardFargment;
import centurybankapp.com.comcenturybankapp.Login.FavouriteFragment.FavouriteFragment;
import centurybankapp.com.comcenturybankapp.Login.QuickpayFragment.QuickPayFragment;

public class BranchAtmAdapter extends FragmentStatePagerAdapter {

    int NumOfTabs;

    public BranchAtmAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
  this.NumOfTabs=NumOfTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return BranchFragment.newInstance();

            case 1:
                return BranchFragment.newInstance();


            default:
               return BranchFragment.newInstance();
        }

    }

    @Override
    public int getCount() {
        return NumOfTabs;
    }
}
