package centurybankapp.com.comcenturybankapp.Login.BranchFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;

import centurybankapp.com.comcenturybankapp.Login.BranchFragment.mvp.BranchPresenter;
import centurybankapp.com.comcenturybankapp.Login.BranchFragment.mvp.Branch_View;
import centurybankapp.com.comcenturybankapp.Login.deps.DaggerDeps;
import centurybankapp.com.comcenturybankapp.Login.deps.Deps;
import centurybankapp.com.comcenturybankapp.Login.networking.NetworkModule;


public class BranchFragment  extends Fragment implements Branch_View{

    Branch_View branchview;
    BranchPresenter branchPresenter;
    Deps deps;
    public static BranchFragment newInstance() {
        BranchFragment fragment = new BranchFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {branchview=this;
        File cacheFile = new File(getActivity().getCacheDir(), "responses");
        deps = DaggerDeps.builder().networkModule(new NetworkModule(cacheFile)).build();
        deps.inject(BranchFragment.this);
        branchPresenter=new BranchPresenter(getActivity(),branchview);
        return branchPresenter;
    }


}
