package centurybankapp.com.comcenturybankapp.Login.DashboardFragment.mvp;

import com.google.gson.annotations.SerializedName;

public class AccountResponse {
    @SerializedName("Status")
    public int Status;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public AccountResponse.Data getData() {
        return Data;
    }

    public void setData(AccountResponse.Data data) {
        Data = data;
    }

    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Data {

    }
}
