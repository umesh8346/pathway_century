package centurybankapp.com.comcenturybankapp.Login.BranchFragment.mvp;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.comcenturybankapp.R;

public class BranchPresenter  extends FrameLayout{
    Branch_View branch_view;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    public BranchPresenter(@NonNull Context context,Branch_View branch_view) {
        super(context);

        inflate(getContext(), R.layout.favourite_layout, this);
        progressBar=(ProgressBar)findViewById(R.id.progressBar);
        recyclerView=(RecyclerView)findViewById(R.id.recycleviewFavourite);
    }
}
