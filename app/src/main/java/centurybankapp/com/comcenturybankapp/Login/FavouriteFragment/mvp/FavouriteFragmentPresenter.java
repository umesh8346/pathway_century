package centurybankapp.com.comcenturybankapp.Login.FavouriteFragment.mvp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.comcenturybankapp.R;

import centurybankapp.com.comcenturybankapp.Login.DashboardFragment.mvp.AccountResponse;
import centurybankapp.com.comcenturybankapp.Login.networking.NetworkError;
import centurybankapp.com.comcenturybankapp.Login.networking.Service;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class FavouriteFragmentPresenter extends FrameLayout {

    FavouriteFragmentView favouriteFragmentView;
    Service service;
    private CompositeSubscription subscriptions;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    public FavouriteFragmentPresenter(@NonNull Context context,
                                      FavouriteFragmentView favouriteFragmentView, Service service) {
        super(context);
        this.favouriteFragmentView=favouriteFragmentView;
        this.service=service;
        this.subscriptions = new CompositeSubscription();
        inflate(getContext(), R.layout.favourite_layout, this);
        progressBar=(ProgressBar)findViewById(R.id.progressBar);
        recyclerView=(RecyclerView)findViewById(R.id.recycleviewFavourite);
        favouriteFragmentView.progressbar(progressBar, View.VISIBLE);
        userFavourite("1");
    }

    public void userFavourite(String UserId){
        Subscription subscription = service.getuserfavourite(new Service.GetFavourite() {
            @Override
            public void onSuccess(FavouriteResponse favouriteResponse) {

                favouriteFragmentView.userfavorite(favouriteResponse,recyclerView);

            }

            @Override
            public void onError(NetworkError networkError) {

            }

        },UserId);

        subscriptions.add(subscription);
    }

}
