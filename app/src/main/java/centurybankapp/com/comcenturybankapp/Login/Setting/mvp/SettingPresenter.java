package centurybankapp.com.comcenturybankapp.Login.Setting.mvp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import com.comcenturybankapp.R;


public class SettingPresenter extends FrameLayout {
    SettingView settingview;
    Toolbar toolbar;
    public SettingPresenter(@NonNull Context context, SettingView settingview) {
        super(context);
        this.settingview=settingview;
        inflate(getContext(), R.layout.change_password_layout, this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        settingview.settoolbar(toolbar,"Setting");
    }
}
