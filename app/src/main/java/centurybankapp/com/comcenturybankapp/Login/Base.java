package centurybankapp.com.comcenturybankapp.Login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.io.File;


import centurybankapp.com.comcenturybankapp.Login.deps.DaggerDeps;
import centurybankapp.com.comcenturybankapp.Login.deps.Deps;
import centurybankapp.com.comcenturybankapp.Login.networking.NetworkModule;


public class Base extends AppCompatActivity {
    Deps deps;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File cacheFile = new File(getCacheDir(), "responses");
        deps = DaggerDeps.builder().networkModule(new NetworkModule(cacheFile)).build();

    }

    public Deps getDeps() {
        return deps;
    }
}
