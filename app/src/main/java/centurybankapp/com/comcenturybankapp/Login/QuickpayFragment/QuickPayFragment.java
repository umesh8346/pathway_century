package centurybankapp.com.comcenturybankapp.Login.QuickpayFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import javax.inject.Inject;


import centurybankapp.com.comcenturybankapp.Login.QuickpayFragment.mvp.QuickPresenter;
import centurybankapp.com.comcenturybankapp.Login.QuickpayFragment.mvp.QuickpayView;
import centurybankapp.com.comcenturybankapp.Login.deps.Deps;

import centurybankapp.com.comcenturybankapp.Login.networking.Service;


public class QuickPayFragment extends Fragment implements QuickpayView {
    QuickpayView quickpayView;
    QuickPresenter quickPresenter;
    Deps deps;
    @Inject
    Service service;

    public static QuickPayFragment newInstance() {
        QuickPayFragment fragment = new QuickPayFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        quickpayView = this;
        quickPresenter=new QuickPresenter(getActivity(),this);
        return quickPresenter;
    }


    @Override
    public void ntcprepaidtopup() {
        System.out.println("click ntc top up");

    }

    @Override
    public void ntcpostpaid() {

    }

    @Override
    public void ntcadsltopup() {

    }
}
