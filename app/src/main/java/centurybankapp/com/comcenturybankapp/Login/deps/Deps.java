package centurybankapp.com.comcenturybankapp.Login.deps;


import android.support.v4.app.FragmentActivity;

import javax.inject.Singleton;

import centurybankapp.com.comcenturybankapp.Login.BranchAtm.BranchAtmActivity;
import centurybankapp.com.comcenturybankapp.Login.BranchAtmListView.BranchAtmListActivity;
import centurybankapp.com.comcenturybankapp.Login.BranchFragment.BranchFragment;
import centurybankapp.com.comcenturybankapp.Login.Dashboard.DashboardActivity;

import centurybankapp.com.comcenturybankapp.Login.DashboardFragment.DashboardFargment;
import centurybankapp.com.comcenturybankapp.Login.FAQ.FAQActivity;
import centurybankapp.com.comcenturybankapp.Login.FavouriteFragment.FavouriteFragment;
import centurybankapp.com.comcenturybankapp.Login.LoginActivity;
import centurybankapp.com.comcenturybankapp.Login.Setting.SettingActivity;
import centurybankapp.com.comcenturybankapp.Login.networking.NetworkModule;
import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class,})
public interface Deps {
  void inject(LoginActivity loginActivity);

  void inject(DashboardActivity dashboardActivity);
  void inject(FAQActivity faqActivity);
  void inject(DashboardFargment dashboardFargment);
  void inject(FavouriteFragment favouriteFragment);
  void inject(BranchAtmActivity branchAtmActivity);
  void inject(SettingActivity settingActivity);
  void inject(BranchAtmListActivity branchAtmListActivity);
  void inject(BranchFragment branchFragment);
}
