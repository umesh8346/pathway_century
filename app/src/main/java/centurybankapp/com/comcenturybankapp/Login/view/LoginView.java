package centurybankapp.com.comcenturybankapp.Login.view;


import android.widget.EditText;
import android.widget.ProgressBar;


public interface LoginView {


    void onLoginResult(Boolean result, int code,String username,String password);

    void onSetProgressBarVisibility(int visibility, ProgressBar progressBar);

    void isnotvalid(EditText ed, String error);

    void getusersuccess(Boolean b);



    void onFailure(String appErrorMessage);
}
