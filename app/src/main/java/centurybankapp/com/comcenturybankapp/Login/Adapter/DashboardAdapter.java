package centurybankapp.com.comcenturybankapp.Login.Adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import centurybankapp.com.comcenturybankapp.Login.DashboardFragment.DashboardFargment;
import centurybankapp.com.comcenturybankapp.Login.FavouriteFragment.FavouriteFragment;
import centurybankapp.com.comcenturybankapp.Login.QuickpayFragment.QuickPayFragment;

public class DashboardAdapter extends FragmentStatePagerAdapter {

    int NumOfTabs;

    public DashboardAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
  this.NumOfTabs=NumOfTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return DashboardFargment.newInstance();

            case 1:
                return QuickPayFragment.newInstance();
            case 2:
                return FavouriteFragment.newInstance();

            default:
               return DashboardFargment.newInstance();
        }

    }

    @Override
    public int getCount() {
        return NumOfTabs;
    }
}
