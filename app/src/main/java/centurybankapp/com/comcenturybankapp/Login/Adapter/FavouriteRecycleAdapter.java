package centurybankapp.com.comcenturybankapp.Login.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.comcenturybankapp.R;

import centurybankapp.com.comcenturybankapp.Login.DashboardFragment.mvp.DashboardFragmentView;
import centurybankapp.com.comcenturybankapp.Login.FavouriteFragment.mvp.FavouriteFragmentView;


public class FavouriteRecycleAdapter extends RecyclerView.Adapter<FavouriteRecycleAdapter.ViewHolder> {
Context mContext;
    FavouriteFragmentView favouriteFragmentView;
    public FavouriteRecycleAdapter(Context c, FavouriteFragmentView favouriteFragmentView) {
        this.mContext = c;
        this.favouriteFragmentView=favouriteFragmentView;
    }

    @Override
    public FavouriteRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        // create a new view
        View vi = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favourite_item_layout, parent, false);
        ViewHolder vh = new ViewHolder(vi);

        vh.btnpay= (Button)vi.findViewById(R.id.btnpay);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.btnpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favouriteFragmentView.paybutton(v,position);
            }
        });



    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

Button btnpay;

        public ViewHolder(View v) {
            super(v);


        }

    }
}
