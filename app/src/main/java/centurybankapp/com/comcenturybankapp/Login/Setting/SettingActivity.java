package centurybankapp.com.comcenturybankapp.Login.Setting;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import centurybankapp.com.comcenturybankapp.Login.Base;
import centurybankapp.com.comcenturybankapp.Login.Setting.mvp.SettingPresenter;
import centurybankapp.com.comcenturybankapp.Login.Setting.mvp.SettingView;



public class SettingActivity extends Base implements SettingView {
    SettingPresenter settingPresenter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDeps().inject(this);
        settingPresenter = new SettingPresenter(this,this);
        setContentView(settingPresenter);
    }

    @Override
    public void settoolbar(Toolbar toolbar, String title) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }
}
