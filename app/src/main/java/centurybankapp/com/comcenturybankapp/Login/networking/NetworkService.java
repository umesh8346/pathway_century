package centurybankapp.com.comcenturybankapp.Login.networking;


import centurybankapp.com.comcenturybankapp.Login.DashboardFragment.mvp.AccountResponse;
import centurybankapp.com.comcenturybankapp.Login.FavouriteFragment.mvp.FavouriteResponse;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;


public interface NetworkService {
    @FormUrlEncoded
    @POST("UserSignIn")
    Observable<centurybankapp.com.comcenturybankapp.Login.networking.Response> getuserlogin(@Field("UserName") String username, @Field("Password") String password);

    @FormUrlEncoded
    @POST("UserSignIn")
    Observable<AccountResponse> getuseraccount(@Field("UserID") String userID);


    @FormUrlEncoded
    @POST("UserSignIn")
    Observable<FavouriteResponse> getuserFavourite(@Field("UserID") String userID);


}
