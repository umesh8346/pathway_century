package centurybankapp.com.comcenturybankapp.Login.FAQ.mvp;

import android.support.v7.widget.Toolbar;
import android.widget.ExpandableListView;


import java.util.HashMap;
import java.util.List;

public interface FAQView {
void expandlistview(ExpandableListView exlist , List<String> parent, HashMap<String, List<String>> child);
    void settoolbar(Toolbar toolbar, String title );
}
