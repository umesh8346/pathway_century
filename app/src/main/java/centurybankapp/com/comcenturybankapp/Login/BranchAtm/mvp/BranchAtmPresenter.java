package centurybankapp.com.comcenturybankapp.Login.BranchAtm.mvp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.comcenturybankapp.R;
import com.ogaclejapan.arclayout.ArcLayout;

import java.util.ArrayList;
import java.util.List;

import centurybankapp.com.comcenturybankapp.Login.networking.Service;
import centurybankapp.com.comcenturybankapp.Login.utils.AnimatorUtils;


public class BranchAtmPresenter extends FrameLayout implements View.OnClickListener {
    BranchAtmView branchAtmView;
ImageButton imagebuttonListbranch;
    Service service;
    FloatingActionButton fab;
    FrameLayout  menu_layout;
    ArcLayout arclayout;
    Toolbar tootlbar;

      public BranchAtmPresenter(@NonNull Context context, BranchAtmView branchAtmView, Service service) {
        super(context);
           this.branchAtmView=branchAtmView;
this.service=service;
          inflate(getContext(), R.layout.branch_atm_layout, this);
          fab=(FloatingActionButton)findViewById(R.id.fab);
          menu_layout=(FrameLayout)findViewById(R.id.menu_layout);
          arclayout = (ArcLayout) findViewById(R.id.arclayout);
          tootlbar=(Toolbar)findViewById(R.id.toolbar);
          imagebuttonListbranch=(ImageButton) findViewById(R.id.imagebuttonListbranch);
          branchAtmView.settoolbar(tootlbar,"Branch and ATM location");
          for (int i = 0, size = arclayout.getChildCount(); i < size; i++) {
              arclayout.getChildAt(i).setOnClickListener(this);
          }
          fab.setOnClickListener(this);
          imagebuttonListbranch.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v==fab){
            onFabClick(v);
            return;
        }
        if(v==imagebuttonListbranch){
            branchAtmView.branchListview();
        }

    }


    private void onFabClick(View v) {
        if (v.isSelected()) {
            hideMenu();
        } else {
            showMenu();
        }
        v.setSelected(!v.isSelected());
    }


    @SuppressWarnings("NewApi")
    private void showMenu() {
        menu_layout.setVisibility(View.VISIBLE);
        List<Animator> animList = new ArrayList<>();
        for (int i = 0, len = arclayout.getChildCount(); i < len; i++) {
            animList.add(createShowItemAnimator(arclayout.getChildAt(i)));
        }
        AnimatorSet animSet = new AnimatorSet();
        animSet.setDuration(400);
        animSet.setInterpolator(new OvershootInterpolator());
        animSet.playTogether(animList);
        animSet.start();
    }

    @SuppressWarnings("NewApi")
    private void hideMenu() {

        List<Animator> animList = new ArrayList<>();

        for (int i = arclayout.getChildCount() - 1; i >= 0; i--) {
            animList.add(createHideItemAnimator(arclayout.getChildAt(i)));
        }

        AnimatorSet animSet = new AnimatorSet();
        animSet.setDuration(400);
        animSet.setInterpolator(new AnticipateInterpolator());
        animSet.playTogether(animList);
        animSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                menu_layout.setVisibility(View.INVISIBLE);
            }
        });
        animSet.start();

    }

    private Animator createHideItemAnimator(final View item) {
        float dx = fab.getX() - item.getX();
        float dy = fab.getY() - item.getY();

        Animator anim = ObjectAnimator.ofPropertyValuesHolder(
                item,
                AnimatorUtils.rotation(720f, 0f),
                AnimatorUtils.translationX(0f, dx),
                AnimatorUtils.translationY(0f, dy)
        );

        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                item.setTranslationX(0f);
                item.setTranslationY(0f);
            }
        });

        return anim;
    }

    private Animator createShowItemAnimator(View item) {

        float dx = fab.getX() - item.getX();
        float dy = fab.getY() - item.getY();

        item.setRotation(0f);
        item.setTranslationX(dx);
        item.setTranslationY(dy);

        Animator anim = ObjectAnimator.ofPropertyValuesHolder(
                item,
                AnimatorUtils.rotation(0f, 720f),
                AnimatorUtils.translationX(dx, 0f),
                AnimatorUtils.translationY(dy, 0f)
        );

        return anim;
    }
}
