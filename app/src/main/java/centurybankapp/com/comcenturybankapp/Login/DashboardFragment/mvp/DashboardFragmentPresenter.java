package centurybankapp.com.comcenturybankapp.Login.DashboardFragment.mvp;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.comcenturybankapp.R;

import centurybankapp.com.comcenturybankapp.Login.networking.NetworkError;

import centurybankapp.com.comcenturybankapp.Login.networking.Service;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class DashboardFragmentPresenter extends FrameLayout {
    TextView textviewMessage;
    DashboardFragmentView dashboardFragmentView;
    RecyclerView recycleviewMyaccount,recycleviewMydemat;
    ProgressBar progressBarDashboard;
    Service service;
    private CompositeSubscription subscriptions;

    public DashboardFragmentPresenter(@NonNull Context context,DashboardFragmentView dashboardFragmentView,Service servise) {
        super(context);
        this.dashboardFragmentView=dashboardFragmentView;
        this.service=servise;
        this.subscriptions = new CompositeSubscription();
        inflate(getContext(), R.layout.main_dashboard_layout, this);
        textviewMessage=(TextView)findViewById(R.id.textviewMessage);
        textviewMessage.setSelected(true);
        recycleviewMyaccount=(RecyclerView)findViewById(R.id.recycleviewMyaccount);
        recycleviewMydemat=(RecyclerView)findViewById(R.id.recycleviewMydemat);
        progressBarDashboard=(ProgressBar)findViewById( R.id.progressBarDashboard);
        dashboardFragmentView.onSetProgressBarVisibility(progressBarDashboard, View.VISIBLE);
        useraccount("1");
    }


    public void useraccount(String UserId){

        Subscription subscription = service.getuseraccount(new Service.GetAccountCallback() {
            @Override
            public void onSuccess(AccountResponse AccontResponse) {

            dashboardFragmentView.getmyaccount(AccontResponse,recycleviewMyaccount);
                dashboardFragmentView.getmyaccount(AccontResponse,recycleviewMyaccount);
            }

            @Override
            public void onError(NetworkError networkError) {

            }

        },UserId);

        subscriptions.add(subscription);
    }
}
