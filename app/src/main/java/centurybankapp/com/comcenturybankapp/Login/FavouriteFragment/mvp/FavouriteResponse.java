package centurybankapp.com.comcenturybankapp.Login.FavouriteFragment.mvp;

import com.google.gson.annotations.SerializedName;

public class FavouriteResponse {
    @SerializedName("Status")
    public int Status;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public FavouriteResponse.Data getData() {
        return Data;
    }

    public void setData(FavouriteResponse.Data data) {
        Data = data;
    }

    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Data {

    }
}
