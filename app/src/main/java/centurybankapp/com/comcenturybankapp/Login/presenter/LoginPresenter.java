package centurybankapp.com.comcenturybankapp.Login.presenter;




public interface LoginPresenter {

    void clear();
    void doLogin(String name, String passwd);
    void setProgressBarVisiblity(int visiblity);
    void getuserlogin(String name, String password);
}
