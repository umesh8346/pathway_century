package centurybankapp.com.comcenturybankapp.Login.presenter;



import android.app.Activity;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.comcenturybankapp.R;

import centurybankapp.com.comcenturybankapp.Login.model.UserModel;

import centurybankapp.com.comcenturybankapp.Login.networking.NetworkError;
import centurybankapp.com.comcenturybankapp.Login.networking.NetworkService;
import centurybankapp.com.comcenturybankapp.Login.networking.Response;
import centurybankapp.com.comcenturybankapp.Login.networking.Service;
import centurybankapp.com.comcenturybankapp.Login.view.LoginView;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class LoginPresenterComp extends FrameLayout implements View.OnClickListener{
    private CompositeSubscription subscriptions;
    LoginView loginView;
    UserModel user;
    Service service;
    NetworkService networkService;
    EditText edittextUsername,edittextpassword;
    Button btnLogin;
    ProgressBar progressbar;
    String usernameString,passwordString;
    public LoginPresenterComp(@NonNull Activity activity, LoginView loginView, Service service, NetworkService networkService) {
        super(activity);
        this.loginView = loginView;
        this.subscriptions = new CompositeSubscription();
        this.service=service;
        this.networkService=networkService;
        inflate(getContext(), R.layout.login_layout, this);
        edittextUsername=(EditText)findViewById(R.id.edittextUsername);
        edittextpassword=(EditText)findViewById(R.id.edittextpassword);
        btnLogin=(Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        progressbar=(ProgressBar)findViewById(R.id.loginProgressBar) ;
    }

    public void doLogin(String name, String password) {
       System.out.println("name=="+name);
        user = new UserModel(name,password);
        Boolean isLoginSuccess = true;
        final int code = user.checkUserValidity(name,password);
        if(code==1){
            loginView.isnotvalid(edittextUsername,"username error");
        }
        else if (code==2){
            loginView.isnotvalid(edittextpassword,"password error");
        }
        if (code!=0) isLoginSuccess = false;

        final Boolean result = isLoginSuccess;

       loginView.onLoginResult(result, code,usernameString,passwordString);

    }


    public void getuserlogin(String username, String password) {
        System.out.println("username==="+username);

        Subscription subscription = service.getuserlogin(new Service.GetloginCallback() {
            @Override
            public void onSuccess(Response UserResponse) {

                if(UserResponse.getStatus()==0){
                    //save data;
loginView.getusersuccess(true);
                }
                else{
loginView.onFailure(UserResponse.getMessage());
                }
            }

            @Override
            public void onError(NetworkError networkError) {

                loginView.onFailure(networkError.getAppErrorMessage());
            }

        },username,password);

        subscriptions.add(subscription);
    }
    public void onStop() {
        subscriptions.unsubscribe();
    }


    @Override
    public void onClick(View v) {
        if(v==btnLogin){
            usernameString=edittextpassword.getText().toString();
            passwordString=edittextpassword.getText().toString();
            loginView.onSetProgressBarVisibility(View.VISIBLE,progressbar);
            doLogin(usernameString,passwordString);
        }

    }
}
